import React, { Fragment } from "react";
import classess from "./Counter.module.css";
import { Box } from "@mui/material";

const Counter = ({ Countertops, setCounter, MR, W, H, Type }) => {
  return (
    <Fragment>
      <Box
        className={classess.Counter1}
        sx={{
          backgroundColor: Countertops === "C1" ? "#02424a" : null,
          marginRight: MR,
        }}
        onClick={() => setCounter("C1")}
      ></Box>
      <Box
        className={classess.Counter2}
        sx={{
          backgroundColor: Countertops === "C2" ? "#02424a" : null,
          marginRight: MR,
        }}
        onClick={() => setCounter("C2")}
      ></Box>
      <Box
        className={classess.Counter3}
        sx={{
          backgroundColor: Countertops === "C3" ? "#02424a" : null,
          marginRight: Type === "JOBS" ? "0px" : MR,
        }}
        onClick={() => setCounter("C3")}
      ></Box>
    </Fragment>
  );
};

export default Counter;
